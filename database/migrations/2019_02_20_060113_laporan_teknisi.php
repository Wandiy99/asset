<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LaporanTeknisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_teknisi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sn', 50);
            $table->string('status', 50)->nullable();
            $table->longText('keluhan')->nullable();
            $table->integer('odo_meter')->nullable();
            $table->string('jumlah_arc', 50)->nullable();
            $table->integer('laporan_master_id');
            $table->timestamp('tgl_laporan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_teknisi');
    }
}
