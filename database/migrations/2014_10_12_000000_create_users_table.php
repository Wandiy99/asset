<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('asset_wmta', function (Blueprint $table) {
            $table->string('serial_number', 50);
            $table->string('product_series', 50);
            $table->string('jenis_barang', 50);
            $table->string('merk_barang', 50);
            $table->string('nama_product', 50);
            $table->string('spesifikasi', 50);
            $table->string('kategori_asset', 50);
            $table->string('nama_gudang', 50);
            $table->string('regional', 50);
            $table->string('fiberzone', 50);
            $table->string('kondisi', 50);
            $table->string('status', 50);
            $table->string('nik_pemakai', 50);
            $table->string('nama_pemakai', 50);
            $table->string('position_name', 50);
            $table->string('group_fungsi', 50);
            $table->string('psa', 50);
            $table->string('create_date', 50);
            $table->string('creator', 50);
            $table->string('no_do', 50);
            $table->string('no_po', 50);
            $table->string('peruntukan', 50);
            $table->string('owner', 50);
            $table->string('id_permintaan', 50);
            $table->string('status_ba', 50);
            $table->string('jenis_asset', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_wmta');
    }
}
