<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Telegram;
use Telegram\Bot\FileUpload\InputFile;
use Validator;
class TeknisiController extends Controller
{
    protected $foto = [
        "KBM_FTTH"      => ["Foto_Kondisi", "Foto_ODO_Meter"],
        "KBM_STATION"   => ["Foto_Kondisi", "Foto_ODO_Meter"],
        "KBM_SKYWORKER" => ["Foto_Kondisi", "Foto_ODO_Meter"],
        "KBM_PICK_UP"   => ["Foto_Kondisi", "Foto_ODO_Meter"],
        "OTDR"          => ["Foto_Charger", "Foto_Battery", "Foto_Fisik_SN"],
        "LAPTOP"        => ["Foto_Kondisi", "Foto_Fisik_SN", "Foto_Charger"],
        "PC"            => ["Foto_Kondisi", "Foto_Fisik_SN", "Foto_Monitor", "Foto_Monitor_SN"],
        "SPLICER"       => ["Foto_Fisik_SN", "Foto_Clipper", "Foto_Stripper", "Foto_Charger", "Foto_Holder", "Foto_Box", "Foto_Jumlah_ARC"],
        "DEFAULT"       => ["Foto_Kondisi"]
    ];
    public function index(){
        $auth = session('auth');
        $data = DB::table('asset_wmta')
            ->select('asset_wmta.*', 
                DB::raw('(select id from laporan_teknisi where sn = asset_wmta.serial_number order by id desc limit 0,1) as last_id,
                    (select tgl_laporan from laporan_teknisi where sn = asset_wmta.serial_number order by id desc limit 0,1) as last_update,
                    (select laporan_master_id from laporan_teknisi where sn = asset_wmta.serial_number order by id desc limit 0,1) as laporan_master_id'))
            ->where('nik_pemakai', $auth->nik)->get();
        $laporan_master = DB::table('laporan_master')->orderBy('id', 'desc')->first();
        //dd($data);
        return view('teknisi.list', compact('data', 'laporan_master'));
    }
    public function update($id, $master_id, $sn){
        $data = DB::table('asset_wmta')->where('serial_number', str_replace('*','/',$sn))->first();
        if(isset($this->foto[str_replace(' ', '_', $data->jenis_barang)])){
            $foto = $this->foto[str_replace(' ', '_', $data->jenis_barang)];
        }else{
            $foto = $this->foto['DEFAULT'];
        }
        $laporan = DB::table('laporan_teknisi')->where('id', $id)->first();
        //dd($laporan);
        //dd($data);
        return view('teknisi.form', compact('data', 'foto', 'laporan'));
    }
    public function save($id, $master_id, $sn, Request $req){
        //dd("a");
        $asset = DB::table('asset_wmta')->where('serial_number', str_replace('*','/',$sn))->first();
        if(isset($this->foto[str_replace(' ', '_', $asset->jenis_barang)])){
            $foto = $this->foto[str_replace(' ', '_', $asset->jenis_barang)];
        }else{
            $foto = $this->foto['DEFAULT'];
        }

        foreach($foto as $f){
            $rulevalidator['flag_'.$f] = 'required';
        }
        $messages = [
            'required' => 'Pilih :attribute.',
        ];
        $validator = Validator::make($req->all(), $rulevalidator, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('alertblock', [
                            ['type' => 'danger', 'text' => 'Upload Kurang Lengkap']
                        ]);
        }

        $exist = DB::table('laporan_teknisi')->where('id', $id)->first();

        //transaction
        DB::transaction(function () use (&$id, $exist, $asset, $master_id, $req, $foto) {
            if($exist){
                //update laporan
                DB::table('laporan_teknisi')->where('id', $id)->update([
                    "sn"                => $asset->serial_number,
                    "keluhan"           => $req->keluhan,
                    "odo_meter"         => $req->odo_meter,
                    "jumlah_arc"        => $req->jumlah_arc,
                    "laporan_master_id" => $master_id,
                    "status"            => $req->status
                ]);
            }else{
                //insert
                $id = DB::table('laporan_teknisi')->insertGetId([
                    "sn"                => $asset->serial_number,
                    "keluhan"           => $req->keluhan,
                    "odo_meter"         => $req->odo_meter,
                    "jumlah_arc"        => $req->jumlah_arc,
                    "laporan_master_id" => $master_id,
                    "status"            => $req->status,
                    "tgl_laporan"       => DB::raw('now()')
                ]);
            }
            $this->handleFileUpload($req, $id, $foto);
        });
        $msg = "<b>LAPORAN KEADAAN ASSET</b>\n====================\n<b>NamaProduct : </b>".$asset->nama_product."\n<b>SN : </b>".$asset->serial_number."\n<b>JENIS : </b>".$asset->jenis_barang."\n<b>PJ : </b>".$asset->nama_pemakai."\n<b>STATUS : </b>".$req->status."\n<b>KELUHAN : </b>".$req->keluhan."";
        Telegram::sendMessage([
            "text"      => $msg,
            "chat_id"   => "-353686037",
            "parse_mode"   => "html"
        ]);
        foreach($foto as $name) {
            $path = public_path().'/storage/'.$id.'/'.$name.'.jpg';
            //dd($path);
            Telegram::sendPhoto([
                'chat_id' => '-353686037', 
                'photo' => InputFile::create($path, $name), 
                'caption' => $name
            ]);
        }
        return redirect('/')->with('alertblock', [
                ['type' => 'success', 'text' => 'Sukses Menyimpan Laporan']
            ]);
    }

    private function handleFileUpload($request, $id, $foto){
        foreach($foto as $name) {
          $input = 'photo-'.$name;
          if ($request->hasFile($input)) {
            //dd($input);
            $path = public_path().'/storage/'.$id.'/';
            if (!file_exists($path)) {
              if (!mkdir($path, 0770, true))
                return redirect()->back()->with('alertblock', [
                    ['type' => 'danger', 'text' => 'Gagal menyiapkan folder']
                ]);
            }
            $file = $request->file($input);
            $ext = 'jpg';
            //TODO: path, move, resize
            try {
              $moved = $file->move("$path", "$name.$ext");
              $img = new \Imagick($moved->getRealPath());
              $img->scaleImage(100, 150, true);
              $img->writeImage("$path/$name-th.$ext");
            }
            catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
              return redirect()->back()->with('alertblock', [
                    ['type' => 'success', 'text' => 'Gagal upload foto']
                ]);
            }
          }
        }
    }
}
