<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
class AjaxJsonController extends Controller
{
    public function getDetailBySn($sn){
        return json_encode(DB::table('asset_wmta')->where('serial_number', str_replace('*','/',$sn))->first());
    }
}
