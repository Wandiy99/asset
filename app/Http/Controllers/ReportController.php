<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Illuminate\Support\Facades\Session;

class ReportController extends Controller
{
    public function report_1(){
	    $last_laporan = DB::table('laporan_master')->orderBy('id','desc')->first()->id;
	    $asset = DB::table('asset_wmta')->whereIn('jenis_barang', ['KBM FTTH','KBM STATION','KBM SKYWORKER','KBM PICK UP','OTDR','LAPTOP','PC','SPLICER', 'SEPEDA MOTOR'])->get();
		$teknisi = DB::select("SELECT COUNT(*) AS Rows, nik_pemakai, nama_pemakai FROM asset_wmta where nik_pemakai!='' and jenis_barang IN('KBM FTTH','KBM STATION','KBM SKYWORKER','KBM PICK UP','OTDR','LAPTOP','PC','SPLICER', 'SEPEDA MOTOR') GROUP BY nik_pemakai ORDER BY Rows desc");
		$laporan = DB::select("SELECT * FROM laporan_teknisi lt left join laporan_master lm on lt.laporan_master_id = lm.id where lm.id = ?", [$last_laporan]);
		//dd($teknisi);
		return view('report.report', compact('asset', 'teknisi', 'laporan'));
	}
}