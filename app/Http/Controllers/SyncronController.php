<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
class SyncronController extends Controller
{
	public static function sync(){
        echo "login\n";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://speakup.telkomakses.co.id/wtmta/index.php?r=site/login');
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
        curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=97150038&LoginForm%5Bpassword%5D=kualakapuas%40123&yt0=");
        $result = curl_exec($ch);
        echo "downloading file\n";
        curl_setopt($ch, CURLOPT_URL, 'https://speakup.telkomakses.co.id/wtmta/index.php?r=masteraset/downloadexcelsn&gudang=50&productseries=&jenis=');
        $result = curl_exec($ch);

        echo "reconstruck file to variable\n";
        $dom = @\DOMDocument::loadHTML(trim($result));
        $table = $dom->getElementsByTagName('table')->item(0);
        $rows = $table->getElementsByTagName('tr');
        $columns = array(
                1 =>'serial_number',
                'product_series',
                'jenis_barang',
                'merk_barang',
                'nama_product',
                'spesifikasi',
                'kategori_asset',
                'nama_gudang',
                'regional',
                'fiberzone',
                'kondisi',
                'status',
                'nik_pemakai',
                'nama_pemakai',
                'position_name',
                'group_fungsi',
                'psa',
                'create_date',
                'creator',
                'no_do',
                'no_po',
                'peruntukan',
                'owner',
                'id_permintaan',
                'status_ba',
                'jenis_asset'
            );
        $result = array();
        for ($i = 2, $count = $rows->length; $i < $count; $i++)
        {
            $cells = $rows->item($i)->getElementsByTagName('td');
            $data = array();
            for ($j = 1, $jcount = count($columns); $j < $jcount; $j++)
            {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
            }
            $result[] = $data;
            $dataarray[] = $data;
        }

        echo "saving to database\n";
        $srcarr=array_chunk($result,500);
        DB::table('asset_wmta')->truncate();
        foreach($srcarr as $item) {
            DB::table('asset_wmta')->insert($item);
        }
        dd($result);
    }
}
