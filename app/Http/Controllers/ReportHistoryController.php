<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Illuminate\Support\Facades\Session;

class ReportHistoryController extends Controller
{

	public function history_report(){
		$rhistory = DB::table('laporan_teknisi')->get();
		$data = DB::table('asset_wmta')->select('*', 'serial_number as text')->get();
		return view ('report.reportHistory', compact('rhistory','data'));
	}

	public function showhistory($sn){
		$sn = str_replace('*', '/', $sn);
		$shistory = DB::select("SELECT sn, (SELECT id FROM laporan_master lm WHERE lm.id = lt.id) AS id, 
		(SELECT tgl FROM laporan_master lm WHERE lm.id = lt.id) AS tgl FROM laporan_teknisi lt WHERE sn = ?", [$sn]);
		$data = DB::table('asset_wmta')->select('*', 'serial_number as text')->get();
		// dd($shistory);
		return view ('report.reportHistory', compact('shistory','data'));
	}
}