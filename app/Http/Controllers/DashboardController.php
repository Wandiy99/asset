<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
	public function listbarang(){
		$listjenis = DB::select ('SELECT COUNT(*) AS total, jenis_barang,
			(select count(*) from asset_wmta aw where aw.jenis_barang=wmta.jenis_barang and kondisi="hilang") as hilang,
			(select count(*) from asset_wmta aw where aw.jenis_barang=wmta.jenis_barang and kondisi="RUSAK") as rusak,
			(select count(*) from asset_wmta aw where aw.jenis_barang=wmta.jenis_barang and kondisi="BAIK") as baik,
			(select count(*) from asset_wmta aw where aw.jenis_barang=wmta.jenis_barang and kondisi="TERMINATE") as terminate, 
			(select count(*) from asset_wmta aw where aw.jenis_barang=wmta.jenis_barang and status="Used") as used, 
			(select count(*) from asset_wmta aw where aw.jenis_barang=wmta.jenis_barang and status="In TAG (Available)") as in_tag, 
			(select count(*) from asset_wmta aw where aw.jenis_barang=wmta.jenis_barang and status="Out TAG") as out_tag, 
			(select count(*) from asset_wmta aw where aw.jenis_barang=wmta.jenis_barang and status="Return / Available") as available 
			FROM asset_wmta wmta GROUP BY jenis_barang ORDER BY jenis_barang'
		);
		return view('user.dashboard', compact('listjenis'));
	}

	public function listcell($jenis_barang, $kondisi, $status){
		$jenis_barang = str_replace('*', '/', $jenis_barang);
		$assetjump = DB::table('asset_wmta');
		if($jenis_barang){
			$assetjump->where('jenis_barang', $jenis_barang);
			// echo"jb";
		}
		if($kondisi){
			$assetjump->where('kondisi', $kondisi);
			// echo"kn";
		}
		if($status){
			$assetjump->where('status',  str_replace('*', '/', $status));
		}
		$assetjump = $assetjump->get();
		

		return view('user.detail',compact('assetjump'));
	}
}