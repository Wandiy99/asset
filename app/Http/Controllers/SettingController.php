<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    public function index(){
	$data = DB::table('laporan_jenis_asset')->select('*', 'jenis_aset as text')->get();

	// foreach ($data as $a) {
	// 	$arrJenisAset[] = $a->jenis_aset;
	// }
	//dd($data);
	return view('setting', compact('data'));
	}

	public function inputjenis($id){
		$inputasset = DB::Table('laporan_jenis_asset')->where ('id'. $id)->first();
		return view('', compact('inputasset'));
	}
}