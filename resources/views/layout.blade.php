<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

  <title>TELKOMAKSES-TOMMAN</title>

  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">

  <!-- Core stylesheets -->
  <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="/css/pixeladmin.min.css" rel="stylesheet" type="text/css">
  <link href="/css/widgets.min.css" rel="stylesheet" type="text/css">

  <!-- Theme -->
  <link href="/css/themes/asphalt.min.css" rel="stylesheet" type="text/css">

  <!-- Pace.js -->
  <script src="/pace/pace.min.js"></script>
  @yield('css')
</head>
<body>
  <!-- Nav -->
  <?php
    $admin = array('97150038', '17920262');

  ?>
  <nav class="px-nav px-nav-left">
    <button type="button" class="px-nav-toggle" data-toggle="px-nav">
      <span class="px-nav-toggle-arrow"></span>
      <span class="navbar-toggle-icon"></span>
      <span class="px-nav-toggle-label font-size-11">HIDE MENU</span>
    </button>

    <ul class="px-nav-content">
      @if(str_contains(session('auth')->nik, $admin))
      <li class="px-nav-item px-nav-dropdown">
        <a href="#"><i class="px-nav-icon ion-gear-b"></i><span class="px-nav-label">Setting</span></a>
        <ul class="px-nav-dropdown-menu">
          <li class="px-nav-item"><a href="/setting"><span class="px-nav-label">Setting</span></a></li>
        </ul>
      </li>
      <li class="px-nav-item px-nav-dropdown">
        <a href="#"><i class="px-nav-icon ion-ios-home"></i><span class="px-nav-label">Dashboard</span></a>
        <ul class="px-nav-dropdown-menu">
          <li class="px-nav-item"><a href="/dashboard"><span class="px-nav-label">Dashboard</span></a></li>
        </ul>
      </li>
      <li class="px-nav-item px-nav-dropdown {{Request ::segment(1) == 'list'?'active' : '' }}">
        <a href="#"><i class="px-nav-icon ion-filing"></i><span class="px-nav-label">Asset</span></a>
        <ul class="px-nav-dropdown-menu">
          <li class="px-nav-item {{Request ::segment(1) == 'list'?'active' : '' }}"><a href="/list"><span class="px-nav-label">List</span></a></li>
        </ul>
      </li>
      <li class="px-nav-item px-nav-dropdown">
        <a href="#"><i class="px-nav-icon ion-clipboard"></i><span class="px-nav-label">Reporting</span></a>
        <ul class="px-nav-dropdown-menu">
          <li class="px-nav-item"><a href="/HistoryReport"><span class="px-nav-label">Cek History Laporan</span></a></li>
        </ul>
        <ul class="px-nav-dropdown-menu">
          <li class="px-nav-item"><a href="/report"><span class="px-nav-label">Report</span></a></li>
        </ul>
      </li>
      @endif
      <li class="px-nav-item px-nav-dropdown">
        <a href="#"><i class="px-nav-icon ion-person"></i><span class="px-nav-label">{{ session('auth')->nama }} <code>{{ session('auth')->nik }}</code></span></a>
        <ul class="px-nav-dropdown-menu">
          <li class="px-nav-item"><a href="/logout"><span class="px-nav-label">Logout</span></a></li>
        </ul>
      </li>
    </ul>
  </nav>

  <!-- Navbar -->
  <nav class="navbar px-navbar">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">Telkomakses</a>
    </div>

    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#px-navbar-collapse" aria-expanded="false"><i class="navbar-toggle-icon"></i></button>

    <div class="collapse navbar-collapse" id="px-navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <form class="navbar-form" role="search" action="/search">
            <div class="form-group">
              <input type="text" name="q" class="form-control" placeholder="Search" style="width: 140px;">
            </div>
          </form>
        </li>
      </ul>
    </div>
  </nav>

  <!-- Content -->
  <div class="px-content">
    <div class="page-header">
      @yield('heading')
    </div>
    <div id="block-alert-with-timer" class="m-b-1"></div>
    <div>
      @yield('content')

    </div>
  </div>

  <!-- Footer -->
  <footer class="px-footer px-footer-bottom">
    Powered by Laravel-Pixeladmin.
  </footer>

  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <!-- Load jQuery -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Core scripts -->
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/pixeladmin.min.js"></script>

  <!-- Your scripts -->
  <script src="/js/app.js"></script>
  <script type="text/javascript">
    $(function(){
      var alertExist = <?= json_encode(Session::has('alerts')); ?>;
      if(alertExist){
        toastr.options = {
          "closeButton": true,
          "positionClass": "toast-top-center",
          "onclick": null,
          "timeOut": "200000"
        }
        var msg = <?= json_encode(session('alerts')[0]); ?>;
        toastr.info(msg.text);
      }

      var alertBlock = <?= json_encode(Session::has('alertblock')); ?>;
      if(alertBlock){
        var $container = $('#block-alert-with-timer');
        var alrt = <?= json_encode(session('alertblock')[0]); ?>;
        $container.pxBlockAlert(alrt.text, { type: alrt.type, style: 'light', timer: 3 });
      }

    });
  </script>
  @yield('js')
</body>
</html>
