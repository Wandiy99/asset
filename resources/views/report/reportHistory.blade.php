@extends('layout')
@section('heading')
    <h1>History Report</h1>
@endsection
@section('content')
    <div class="tab-content">
    <div class="tab-pane active" id="definition">

      <h4 class="doc-subheader">History Report Asset</h4>

      <form>
<form method="post">
  <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="sn" placeholder="Isi Serial Number" 
          value="{{isset($rhistory->sn)?$rhistory->sn : ''}}">
                  </div>
                </div>
  <button type="submit" class="btn btn-lg btn-primary btn-rounded"><i class="fa fa-search">Search</i></button>
  </form>
    </div>
  </div>
<div>
</div>
</br>
@endsection
@section('js')
<script>

</script>
@endsection
