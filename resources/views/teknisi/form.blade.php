@extends('layout')
@section('css')
  <style type="text/css">
    
    .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
    .no-search .select2-search {
      display:none
    }
  </style>
@endsection
@section('heading')
    <h1><a href="/" class="ion-arrow-left-a"></a> Update {{ $data->nama_product }}</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-laporan">
  <div class="form-group form-message-dark">
    <label for="status" class="col-md-2 control-label">Status</label>
    <div class="col-md-9">
      <input type="text" class="form-control status" name="status" id="status" value="{{ isset($laporan->status) ? $laporan->status : '' }}" required>
    </div>
  </div>
  @if($data->jenis_barang=='SPLICER')
  <div class="form-group form-message-dark">
    <label for="jumlah_arc" class="col-md-2 control-label">Jumlah ARC</label>
    <div class="col-md-9">
      <input type="text" class="form-control" placeholder="ARC" name="jumlah_arc" id="jumlah_arc" value="{{ isset($laporan->jumlah_arc) ? $laporan->jumlah_arc : '' }}">
    </div>
  </div>
  @endif
  @if(str_contains(str_replace(' ', '_', $data->jenis_barang), ['KBM_FTTH', 'KBM_STATION', 'KBM_SKYWORKER', 'KBM_PICK_UP']))
  <div class="form-group form-message-dark">
    <label for="odo_meter" class="col-md-2 control-label">ODO Meter</label>
    <div class="col-md-9">
      <input type="text" class="form-control" placeholder="ODO Meter" name="odo_meter" id="odo_meter" value="{{ isset($laporan->odo_meter) ? $laporan->odo_meter : '' }}">
    </div>
  </div>
  @endif
  <div class="form-group form-message-dark">
    <label for="keluhan" class="col-md-2 control-label">Keluhan</label>
    <div class="col-md-9">
      <textarea class="form-control keluhan" placeholder="Keluhan" name="keluhan" id="keluhan">{{ isset($laporan->keluhan) ? $laporan->keluhan : '' }}</textarea>
    </div>
  </div>
  <div class="row text-center col-md-offset-2 input-photos">
      
      @foreach($foto as $input)
        <div class="col-xs-6 col-sm-3">
          <?php
            $path = "/storage/".Request::segment(2)."/$input";
            $th   = "$path-th.jpg";
            $img  = "$path.jpg";
            $flag = "";
            $name = "flag_".$input;
          ?>
          @if (file_exists(public_path().$th))
            <a href="{{ $img }}">
              <img src="{{ $th }}" alt="{{ $input }}" />
            </a>
            <?php
              $flag = 1;
            ?>
          @else
            <img src="/image/placeholder.gif" alt="" />
          @endif
          <br />
          <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
          <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg"/>
          <button type="button" class="btn btn-sm btn-info">
            <i class="ion-camera"></i>
          </button>
          <p>{{ str_replace('_',' ',$input) }}</p>
          {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
        </div>
      @endforeach
  </div>
  <div class="form-group">
    <div class="col-md-offset-2 col-md-9">
      <button type="submit" class="btn">Simpan Laporan</button>
    </div>
  </div>
</form>
@endsection
@section('js')
  <script type="text/javascript">
    $(function() {
      $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          $(inputEl).parent().find('input[type=text]').val(1);
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);
            
          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });

      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });

      $('#form-laporan').pxValidate();
      $('#status').select2({
        placeholder: 'Select Status',
        allowClear: true,
        dropdownCssClass : 'no-search',
        data: [{"id":"Baik", "text" : "Baik"},{"id":"Rusak", "text" : "Rusak"},{"id":"Hilang", "text" : "Hilang"},{"id":"Balik_Nama", "text" : "Balik Nama"}]
      }).change(function() {
        if($(this).val() == 'Baik' || $(this).val() == 'Rusak'){
          $("#keluhan").rules("add", {
            required: true,
            minlength: 2
          });
        }else{
           $("#keluhan").rules("remove");
           $("#keluhan").valid();
        }
        $(this).valid();
      });

      // validator
    });

  </script>
@endsection