@extends('layout')
@section('heading')
    <h1>Inbox Asset {{ session('auth')->nama }}</h1>
@endsection
@section('content')
  <div class="widget-pricing widget-pricing-expanded">
    <div class="widget-pricing-inner row">
     @foreach($data as $no => $d)
        <div class="col-xs-6 col-md-6 col-lg-3 col-xl-3">
          <div class="panel text-xs-center p-y-1">
            <b class="label {{ ($d->laporan_master_id == $laporan_master->id) ? 'label-primary' : 'label-danger' }} label-pill label-outline m-b-2">
              {{ (strlen($d->jenis_barang)>19)?substr($d->jenis_barang,0,19).'...':$d->jenis_barang }}
            </b><br/>
            <span class="label label-info label-outline">{{ (strlen($d->serial_number)>19)?substr($d->serial_number,0,19).'..':$d->serial_number }}</span><br>
            <span class="label label-info label-outline" style="margin:2px 0 2px 0;">{{ (strlen($d->product_series)>19)?substr($d->product_series,0,19).'..':$d->product_series }}</span><br>
            <span class="label label-info label-outline m-b-2">{{ $d->last_update }}</span><br/>

            <a href="#" class="btn btn-xs btn-info detail" data-sn="{{ str_replace('/','*',$d->serial_number) }}"><i class="ion-information"></i>&nbsp;&nbsp;Detail</a>
            <a href="/lapor/{{ ($d->laporan_master_id == $laporan_master->id) ? $d->last_id : 'input' }}/{{ $laporan_master->id }}/{{ str_replace('/','*',$d->serial_number) }}" class="btn btn-xs btn-primary"><i class="{{ ($d->laporan_master_id == $laporan_master->id) ? 'ion-checkmark-round' : 'ion-compose' }}"></i>&nbsp;&nbsp;{{ ($d->laporan_master_id == $laporan_master->id) ? 'Update' : 'Lapor' }}</a>
          </div>
        </div>
      @endforeach
    </div>
  </div>

  <div id="modal-info" class="modal fade modal-alert modal-info">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header"><i class="ion-information-circled"></i></div>
        <div id="detilContent"></div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
  <script type="text/javascript">
    $(function() {
      $(".detail").click(function(e){
        var sn = e.target.dataset.sn;
        var url = "/getDetail/"+sn;
        $.getJSON(url, function(r){
          console.log(r);
          $("#detilContent").html('<div class="modal-title">'+r.jenis_barang+'</div><div class="modal-body"><p>Serial Number : '+r.serial_number+'</p><p>Product Series : '+r.product_series+'</p><p>Merk : '+r.merk_barang+'</p><p>Kategori : '+r.kategori_asset+'</p><p>Creator : '+r.creator+'</p><p>Create Date : '+r.create_date+'</p></div>');
          $("#modal-info").modal('show');
        });
      });
    });
  </script>
@endsection