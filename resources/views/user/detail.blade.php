@extends('layout')
@section('heading')
<h1> <h1><span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Setting / </span>Laporan</h1></h1>
@endsection
@section('content')
<div class="tab-content">
	<div class="tab-pane active" id="definition">
	</div>
</div>
</div>
</br>

<div class="table-primary">
	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="datatables">
			<thead>
				<tr>
					<th>Serial Number</th>
					<th>Product Series</th>
					<th>Jenis Barang</th>
					<th>Merk Barang</th>
					<th>Nama Product</th>
					<th>Spesifikasi</th>
					<th>Kategori Asset</th>
					<th>Nama Gudang</th>
					<th>Regional</th>
					<th>Fiberzone</th>
					<th>Kondisi</th>
					<th>Status</th>
					<th>NIK Pemakai</th>
					<th>Nama Pemakai</th>
					<th>Position Name</th>
					<th>Group Fungsi</th>
					<th>PSA</th>
					<th>Create Date</th>
					<th>Creator</th>
					<th>NO_DO</th>
					<th>NO_PO</th>
					<th>Peruntukan</th>
					<th>Owner</th>
					<th>ID Permintaan</th>
					<th>Status BA</th>
					<th>Jenis Asset</th>
					<th>Action</th>
				</tr>
				<tbody>
					@foreach($assetjump as $no => $dataasset)
					<tr>
						<th><a href="/HistoryReport/?sn={{ str_replace('/', '*', $dataasset	->serial_number) }}/">{{ $dataasset->serial_number}}</a></th>
						<th>{{ $dataasset->product_series}}</th>
						<th>{{ $dataasset->jenis_barang}}</th>
						<th>{{ $dataasset->merk_barang}}</th>
						<th>{{ $dataasset->nama_product}}</th>
						<th>{{ $dataasset->spesifikasi}}</th>
						<th>{{ $dataasset->kategori_asset}}</th>
						<th>{{ $dataasset->nama_gudang}}</th>
						<th>{{ $dataasset->regional}}</th>
						<th>{{ $dataasset->fiberzone}}</th>
						<th>{{ $dataasset->kondisi}}</th>
						<th>{{ $dataasset->status}}</th>
						<th>{{ $dataasset->nik_pemakai}}</th>
						<th>{{ $dataasset->nama_pemakai}}</th>
						<th>{{ $dataasset->position_name}}</th>
						<th>{{ $dataasset->group_fungsi}}</th>
						<th>{{ $dataasset->psa}}</th>
						<th>{{ $dataasset->create_date}}</th>
						<th>{{ $dataasset->creator}}</th>
						<th>{{ $dataasset->no_do}}</th>
						<th>{{ $dataasset->no_po}}</th>
						<th>{{ $dataasset->peruntukan}}</th>
						<th>{{ $dataasset->owner}}</th>
						<th>{{ $dataasset->id_permintaan}}</th>
						<th>{{ $dataasset->status_ba}}</th>
						<th>{{ $dataasset->jenis_asset}}</th>
						<th>{{ $dataasset->jenis_asset}}</th>
						@endforeach
					</tbody>
				</thead> 
			</table>
		</div>
	</div>
	@endsection
	@section('js')
	<script>
		$(function() {
			$('#datatables').dataTable();
			$('#datatables_wrapper .table-caption').text('Asset');
			$('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
	</script>
	@endsection