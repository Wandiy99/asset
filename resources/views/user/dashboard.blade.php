@extends('layout')
@section('heading')
<h1> <h1><span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Setting / </span>Laporan</h1></h1>
@endsection
@section('content')
<div class="tab-content">
	<div class="tab-pane active" id="definition">
	</div>
</div>
</div>
</br>

<div class="table-primary">
	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="datatables">
			<thead>
				<tr>
					<th rowspan="1">Nama Barang</th>
					<th colspan="4">Kondisi</th>
					<th colspan="4">Status</th>
					<th rowspan="2">Total</th>
				</tr>
				<tr>
					<th colspan="1"></th>
					<th>Baik</th>
					<th>Rusak</th>
					<th>Hilang</th>
					<th>Terminate</th>
					<th>IN TAG</th>
					<th>OUT TAG</th>
					<th>Available</th>
					<th>Used</th>
				</tr>
					<tbody>
			@foreach($listjenis as $no => $data)
			<tr>
				<th>{{ $data->jenis_barang }}</th>
				<th><a href="/dashboard/{{ str_replace('/', '*', $data->jenis_barang) }}/BAIK/0">{{ $data->baik }}</a></th>
				<th><a href="/dashboard/{{ str_replace('/', '*', $data->jenis_barang) }}/RUSAK/0">{{ $data->rusak }}</a></th>
				<th><a href="/dashboard/{{ str_replace('/', '*', $data->jenis_barang) }}/HILANG/0">{{ $data->hilang }}</a></th>
				<th><a href="/dashboard/{{ str_replace('/', '*', $data->jenis_barang) }}/TERMINATE/0">{{ $data->terminate }}</a></th>
				<th><a href="/dashboard/{{ str_replace('/', '*', $data->jenis_barang) }}/0/In TAG (Available)">{{ $data->in_tag }}</a></th>
				<th><a href="/dashboard/{{ str_replace('/', '*', $data->jenis_barang) }}/0/Out TAG">{{ $data->out_tag }}</a></th>
				<th><a href="/dashboard/{{ str_replace('/', '*', $data->jenis_barang) }}/0/Return * Available">{{ $data->available }}</a></th>
				<th><a href="/dashboard/{{ str_replace('/', '*', $data->jenis_barang) }}/0/Used">{{ $data->used }}</a></th>
				<th><a href="/dashboard/{{ str_replace('/', '*', $data->jenis_barang) }}/0/0">{{ $data->total }}</a></th>
				@endforeach
			</tbody>
			</thead> 
		</table>
	</div>
</div>
@endsection
@section('js')
<script>
	$(function() {
		$('#datatables').dataTable();
		$('#datatables_wrapper .table-caption').text('Asset');
		$('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
	});
</script>
@endsection