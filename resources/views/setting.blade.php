@extends('layout')
@section('heading')
    <h1> <h1><span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Dashboard / </span>Laporan</h1></h1>
@endsection
@section('content')
    <div class="tab-content">
    <div class="tab-pane active" id="definition">

      <h4 class="doc-subheader">Pilih Serial Number</h4>

      <div class="example">
<select id="select2-base" class="form-control" style="width: 50%" data-allow-clear="true">
</select>
      </div>
    </div>
  </div>
<div>
  <button type="submit" class="btn btn-lg btn-primary btn-rounded">Save</button>
</div>
</br>

<div class="table-primary">
  <table class="table table-striped table-bordered" id="datatables">
    <thead>
      <tr>
        <th>ID Asset</th>
        <th>Jenis Asset</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $idjenis => $inputasset)
       <tr>
      <th>{{$inputasset->id}}</th>
      <th>{{$inputasset->jenis_aset}}</th>
      <th><a href="" class ="btn btn-info btn-sm"><i class="fas fa-edit fa-sm text-white-50"></i>Edit</a>
          <a href="" class = "btn btn-danger btn-sm"><i class="fas fa-trash fa-sm text-white-50"></i>Delete</a></th>
    </tr>
    @endforeach
    </tbody>
  </table>
</div>
@endsection
@section('js')
<script>
  $(function() {
    var data = <?= json_encode($data); ?>;
    console.log(data);
    $('#select2-base').select2({
      data:data,
      placeholder: 'Select value'
      
    });
  });
</script>
<script>
  $(function() {
    $('#datatables').dataTable();
    $('#datatables_wrapper .table-caption').text('Asset');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
  });
</script>
@endsection