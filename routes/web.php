<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('login');
});
Route::post('/login', 'UserController@login');
Route::group(['middleware' => 'CustomAuth'], function(){

	Route::get('/', 'TeknisiController@index');
	Route::get('/lapor/{id}/{master_id}/{sn}', 'TeknisiController@update');
	Route::post('/lapor/{id}/{master_id}/{sn}', 'TeknisiController@save');
	Route::get('/logout', 'UserController@logout');
	Route::get('/list', 'ListController@list');
	Route::get('/setting', 'SettingController@index');
	Route::get('/report', 'ReportController@report_1');
	Route::get('/getDetail/{sn}', 'AjaxJsonController@getDetailBySn');
	Route::get('/dashboard', 'DashboardController@listbarang');
	Route::get('/dashboard/{jenis_barang}', 'DashboardController@listcell');
	Route::get('/dashboard/{jenis_barang}/{kondisi}', 'DashboardController@listcell');
	Route::get('/dashboard/{jenis_barang}/{kondisi}/{status}', 'DashboardController@listcell');
	Route::get('/HistoryReport', 'ReportHistoryController@history_report');
	Route::get('/HistoryReport/?sn={sn}', 'ReportHistoryController@showhistory');
});